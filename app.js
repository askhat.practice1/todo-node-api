// Import necessary modules
const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const morgan = require("morgan");
const dotenv = require("dotenv");
const mongoose = require("mongoose");

// Load environment variables from .env file
dotenv.config();

// Import local modules
const { mongo_db_url } = require("./src/config/database");
const routes = require("./src/routes");
const notFound = require('./src/middleware/not-found');
const errorHandlerMiddleware = require('./src/middleware/error-handler');

// logging environment variables
// console.log(process.env.MONGO_USERNAME);
// console.log(process.env.MONGO_PASSWORD);
// console.log(process.env.MONGO_HOST);
// console.log(process.env.MONGO_PORT);
// console.log(process.env.MONGO_DB);
// console.log(process.env.HOST);
// console.log(process.env.PORT);
// console.log(process.env.MONGO_URL);
// console.log(process.env.JWT_SECRET);
// console.log(url);

// Connect to MongoDB database
mongoose
  .connect(`${mongo_db_url}`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("MongoDB connected"))
  .catch((error) => console.error("MongoDB connection error", error));

// Create a new Express app instance
const app = express();

// Use middleware to parse request bodies and enable CORS
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());

// Log HTTP requests to the console
app.use(morgan("tiny"));

// Use routes defined in the routes folder
app.use("/api/v1", routes);

app.use(notFound);
app.use(errorHandlerMiddleware);

// Start the server
const port = process.env.PORT || 3000;
const host = process.env.HOST || "0.0.0.0";
app.listen(port, host, () => {
  console.log(`Server listening on http://${host}:${port}`);
});
