const moment = require("moment");

async function logger(req, res, next) {
  console.log(
    `${moment().format("YYYY-MM-DD HH:mm:ss")} ${req.method} ${req.url} ${
      req.ip
    }`
  );
  next();
}

module.exports = { logger };
