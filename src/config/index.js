const databaseConfig = require("./database");
const jwtConfig = require("./jwt");

module.exports = {
  database: databaseConfig,
  jwt: jwtConfig,
};

