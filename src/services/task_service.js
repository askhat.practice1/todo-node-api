const { TaskModel } = require("../models");

// tasks
async function create_task_service_function(userId, { title, description }) {
  const existingTask = await TaskModel.findOne({ userId, title });
  if (existingTask) {
    throw new Error("A task with that title already exists");
  }

  const task = new TaskModel({ userId, title, description });
  await task.save();
  return task;
}

async function get_all_tasks_service_function(userId) {
  const tasks = await TaskModel.find({ userId, completed: false });
  console.log(userId, tasks);
  return tasks;
}

async function get_all_completed_tasks_service_function(userId) {
  const tasks = await TaskModel.find({ userId, completed: true });
  return tasks;
}

// task by id
async function get_task_by_id_service_function(userId, _id) {
  console.log(userId, _id);
  const task = await TaskModel.findOne({ userId, _id });
  return task;
}

async function update_task_by_task_id_service_function(
  userId,
  _id,
  { title, description }
) {
  const existingTask = await TaskModel.findOne({ userId, title });
  if (existingTask && existingTask.id !== _id) {
    throw new Error("A task with that title already exists");
  }
  const task = await TaskModel.findByIdAndUpdate(
    _id,
    { title, description },
    { new: true, runValidators: true }
  );
  return task;
}

async function complete_task_by_task_id_service_function(userId, _id) {
  console.log(userId, _id)
  const task = await TaskModel.findOneAndUpdate(
    { userId, _id },
    { completed: true },
    { new: true }
  );
  if (!task) {
    throw new Error("Task not found");
  }
  return task;
}

async function delete_task_service_function(userId, taskId) {
  await TaskModel.deleteOne({ userId, _id: taskId });
}

module.exports = {
  create_task_service_function,
  get_all_tasks_service_function,
  get_all_completed_tasks_service_function,
  get_task_by_id_service_function,
  update_task_by_task_id_service_function,
  complete_task_by_task_id_service_function,
  delete_task_service_function,
};
