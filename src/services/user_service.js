const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { UserModel } = require("../models");

async function create_user({ email, password, first_name, last_name }) {
  const user = new UserModel({
    email,
    password: await bcrypt.hash(password, 10),
    first_name,
    last_name,
  });
  await user.save();
  return user;
}

async function findByEmail(email) {
  const user = await UserModel.findOne({ email });
  return user;
}

async function findById(id) {
  const user = await UserModel.findById(id);
  return user;
}

async function login({ email, password }) {
  const user = await this.findByEmail(email);
  if (!user) {
    throw new Error("Invalid email");
  }
  const match = await bcrypt.compare(password, user.password);
  if (!match) {
    throw new Error("Invalid password");
  }
  const token = jwt.sign({ id: user.id }, process.env.JWT_SECRET, {
    expiresIn: "7d",
  });
  return { token, user };
}

module.exports = { create_user, findByEmail, findById, login };
