const {
  createUserValidator,
  loginUserValidator,
} = require("./user_validators");
const {
  createTaskValidator,
  updateTaskValidator,
} = require("./task_validators");

module.exports = {
  createUserValidator,
  loginUserValidator,
  createTaskValidator,
  updateTaskValidator,
};
