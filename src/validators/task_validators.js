const Joi = require("joi");

const createTaskValidator = (req, res, next) => {
  const schema = Joi.object({
    title: Joi.string().required(),
    description: Joi.string().required(),
  });
  validateRequest(req, next, schema);
};

const updateTaskValidator = (req, res, next) => {
  const schema = Joi.object({
    title: Joi.string().optional(),
    description: Joi.string().optional(),
    completed: Joi.boolean().optional(),
  });
  validateRequest(req, next, schema);
};

const validateRequest = (req, next, schema) => {
  const options = {
    abortEarly: false,
    allowUnknown: true,
    stripUnknown: true,
  };
  const { error, value } = schema.validate(req.body, options);
  if (error) {
    const errorMessage = error.details.map((error) => error.message).join(", ");
    next(`Validation error: ${errorMessage}`);
  } else {
    req.body = value;
    next();
  }
};

module.exports = {
  createTaskValidator,
  updateTaskValidator,
};
