const TaskModel = require("./tasks_model");
const UserModel = require("./user_model");

module.exports = { UserModel, TaskModel };
