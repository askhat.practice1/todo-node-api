const express = require("express");
const router = express.Router();

const users_router = require("./users_router");
const tasks_router = require("./tasks_router");
const task_router = require("./task_router");

router.use("/users", users_router);
router.use("/tasks", tasks_router);
router.use("/task", task_router);

module.exports = router;
