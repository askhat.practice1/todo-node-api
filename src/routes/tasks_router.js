const express = require("express");
const router = express.Router();
const { createTaskValidator } = require("../validators");
const { authenticateUser } = require("../middleware");
const { tasks_controller } = require("../controllers");
const { logger } = require("../middleware");

const { task_create, tasks_get_all, tasks_completed_get_all } =
  tasks_controller;

router.post("/", logger, authenticateUser, createTaskValidator, task_create);
router.get("/", logger, authenticateUser, tasks_get_all);
router.get("/completed/", logger, authenticateUser, tasks_completed_get_all);

module.exports = router;
