const express = require("express");
const router = express.Router();
const { logger } = require("../middleware");

const { users_controller } = require("../controllers");
const { register_user, login_user } = users_controller;
const { createUserValidator, loginUserValidator } = require("../validators");

router.post("/register", logger, createUserValidator, register_user);
router.post("/login", logger, loginUserValidator, login_user);

module.exports = router;
