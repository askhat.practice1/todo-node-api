const express = require("express");
const router = express.Router();

const { authenticateUser } = require("../middleware");
const { updateTaskValidator } = require("../validators");
const { task_controller } = require("../controllers");
const { logger } = require("../middleware");
const {
  task_get_by_taskId,
  task_put_by_taskId,
  task_patch_by_taskId_completed,
  delete_task_by_taskId,
} = task_controller;

router.get("/:taskId", logger, authenticateUser, task_get_by_taskId);
router.put(
  "/:taskId",
  logger,
  authenticateUser,
  updateTaskValidator,
  task_put_by_taskId
);
router.patch(
  "/:taskId/completed",
  logger,
  authenticateUser,
  task_patch_by_taskId_completed
);
router.delete("/:taskId", logger, authenticateUser, delete_task_by_taskId);

module.exports = router;
