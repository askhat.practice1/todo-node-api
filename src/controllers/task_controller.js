const TasksService = require("../services/task_service");
const { validationResult } = require("express-validator");
const { createCustomError } = require("../errors/custom-error")

// Get a task from a user's todo list
async function task_get_by_taskId(req, res, next) {
  try {
    const task = await TasksService.get_task_by_id_service_function(req.user.id, req.params.taskId);
    res.json(task);
  } catch (error) {
    next(error);
  }
}

// Update a task on a user's todo list
async function task_put_by_taskId(req, res, next) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const task = await TasksService.update_task_by_task_id_service_function(
      req.user.id,
      req.params.taskId,
      req.body
    );
    res.json(task);
  } catch (error) {
    next(error);
  }
}

// Mark a task as done on a user's todo list
async function task_patch_by_taskId_completed(req, res, next) {
  try {
    const task = await TasksService.complete_task_by_task_id_service_function(req.user.id, req.params.taskId);
    res.json(task);
  } catch (error) {
    next(error);
  }
}

// Delete a task from a user's todo list
async function delete_task_by_taskId(req, res, next) {
  try {
    await TasksService.delete_task_service_function(req.user.id, req.params.taskId);
    res.sendStatus(204);
  } catch (error) {
    next(error);
  }
}

module.exports = {
  task_get_by_taskId,
  task_put_by_taskId,
  task_patch_by_taskId_completed,
  delete_task_by_taskId,
};
