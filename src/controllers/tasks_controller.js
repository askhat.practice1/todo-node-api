const TasksService = require("../services/task_service");
const { validationResult } = require("express-validator");

// Create a new task and add it to a user's todo list
async function task_create(req, res, next) {
  try {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }
    const task = await TasksService.create_task_service_function(
      req.user.id,
      req.body
    );
    res.status(201).json(task);
  } catch (error) {
    next(error);
  }
}

// Get all tasks from a user's todo list
async function tasks_get_all(req, res, next) {
  try {
    const tasks = await TasksService.get_all_tasks_service_function(req.user.id);
    res.json(tasks);
  } catch (error) {
    next(error);
  }
}

// Get all completed tasks from a user's todo list
async function tasks_completed_get_all(req, res, next) {
  try {
    const tasks = await TasksService.get_all_completed_tasks_service_function(req.user.id);
    res.json(tasks);
  } catch (error) {
    next(error);
  }
}

module.exports = {
  task_create,
  tasks_get_all,
  tasks_completed_get_all,
};
