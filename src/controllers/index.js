const task_controller = require("./task_controller");
const tasks_controller = require("./tasks_controller");
const users_controller = require("./users_controller");

module.exports = { task_controller, tasks_controller, users_controller };
